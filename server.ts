import { connect } from 'https://deno.land/x/redis/mod.ts';
const port = 8080;

const redis = await connect({
    hostname: 'localhost',
    port: 6379,
});

const handler = async (request: Request): Response => {
    return await handleRequest(request)
};

async function handleRequest(request: Request) {
    const url = request.url.split('/').slice(3)
    let name = url.shift()

    switch (name) {
        case 'getkey' 	: return await returnKey(url); break
		case 'getlist' 	: return await returnList(url); break
		case 'addtoset' : return await addToSet(request); break
		case 'getset'   : return await returnSet(url); break
		default 		: return new Response('incorrect endpoint', { status: 500 }); break
    }
}

async function returnKey(args: string[]) {
    if (args.length < 1) {
        return new Response('Not enough arguments', { status: 500 })
    }

    const key 		= args.pop()

    const value 	= await redis.get(key)
    const body 		= value
    const response 	= new Response(body, { status: 200 })

	return response
}

async function returnList(args: string[]) {
    if (args.length < 1) {
        return new Response('Not enough arguments', {
            status: 500
        })
    }

    const key 		= args.pop()
    const length    = await redis.llen(key)
    const elems     = await redis.lrange(key, 0, length-1)
    const body 		= JSON.stringify(elems)
    const response 	= new Response(body, {
        status: 200,
        headers: {
            "Access-Control-Allow-Origin" : "http://chauyoutube.local"
        }
    })

	return response
}

async function returnSet(args: string[]) {
    if (args.length < 1) {
        return new Response('Not enough arguments', {
            status: 500
        })
    }

    const key 		= args.pop()
    const members   = await redis.smembers(key)
    const response 	= new Response(JSON.stringify(members), {
        status: 200,
        headers: {
            "Access-Control-Allow-Origin" : "http://chauyoutube.local"
        }
    })

	return response
}

async function addToSet(request: Request) {
    const requestData = await request.formData()
    const setName = requestData.get('setName') 
    const value = requestData.get('value') 

    await redis.srem(setName, value)
    await redis.sadd(setName, value)
    return new Response('ok', {
        status: 200,
        headers: {
            "Access-Control-Allow-Origin" : "http://chauyoutube.local"
        }
    })
}

console.log(`HTTP server running. Access it at: http://localhost:8080/`);
Deno.serve({ port }, handler);
